package de.unidue.iem.tdr.nis.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Diese Klasse ermöglicht das Abrufen von Aufgaben vom Server und die
 * Implementierung der dazugehörigen Lösungen.
 * <p>
 * Nähere Informationen zu den anderen Klassen und den einzelnen Aufgabentypen
 * entnehmen Sie bitte der entsprechenden Dokumentation im TMT und den Javadocs
 * zu den anderen Klassen.
 *
 * @see Connection
 * @see TaskObject
 */
public class Client implements TaskDefs {
    private Connection con;

    /* hier bitte das Token eintragen */
    private static final String token = "ab69cb4651260bfabc5d29dca0a5985f2ab8cdf1a1b494a55e8b94cc08d9c975";

    /* Aufgaben, die bearbeitet werden sollen */
    private final int[] tasks = {TASK_CLEARTEXT, TASK_XOR, TASK_MODULO,
            TASK_FACTORIZATION, TASK_VIGENERE, TASK_DES_KEYSCHEDULE,
            TASK_DES_RBLOCK, TASK_DES_FEISTEL, TASK_DES_ROUND, TASK_AES_GF8,
            TASK_AES_KEYEXPANSION, TASK_AES_MIXCOLUMNS,
            TASK_AES_TRANSFORMATION, TASK_AES_3ROUNDS, TASK_RC4_LOOP,
            TASK_RC4_KEYSCHEDULE, TASK_RC4_ENCRYPTION, TASK_DIFFIEHELLMAN,
            TASK_RSA_ENCRYPTION, TASK_RSA_DECRYPTION, TASK_ELGAMAL_ENCRYPTION,
            TASK_ELGAMAL_DECRYPTION};

    /**
     * Klassenkonstruktor. Baut die Verbindung zum Server auf.
     */
    public Client() {
        con = new Connection();
        if (con.auth(token))
            System.out.println("Anmeldung erfolgreich.");
        else
            System.out.println("Anmeldung nicht erfolgreich.");
    }

    /**
     * Durchläuft eine Liste von Aufgaben und fordert diese vom Server an.
     */
 
    
    
	public void taskLoop() {
    	
    	
        TaskObject currentTask;
//        TaskObject currentTask1 = null;
//        con.sendMoreParams(currentTask1, pubkeymore(currentTask1.getIntArray(0), currentTask1.getIntArray(1), currentTask1.getStringArray(0)));
        for (int task : tasks) {
            String solution = "Nicht implementiert!";
            currentTask = con.getTask(task);
            switch (task) {
                case TASK_CLEARTEXT:
                    solution = currentTask.getStringArray(0);
                    break;
                case TASK_XOR:
                	 
              solution  = xor( currentTask.getStringArray(0),currentTask.getStringArray(1));
                  
                   break;
              case TASK_MODULO:
                   solution= modulo(currentTask.getIntArray(0), currentTask.getIntArray(1));
                   break;
               case TASK_FACTORIZATION:
                   solution= faktorisierung(currentTask.getIntArray(0));  
                 //  System.out.println(currentTask.getIntArray(0) + solution);
                   break;
               case TASK_VIGENERE:
                  solution= vignere(currentTask.getStringArray(0), currentTask.getStringArray(1));
                    break;
                    
               case TASK_DES_KEYSCHEDULE:            	  
                          	  solution = DesKeySchedule(currentTask.getStringArray(0), currentTask.getIntArray(0));
                          	
                  break;
                  
               case TASK_DES_RBLOCK:
               	 solution =getblockr(currentTask.getStringArray(0), currentTask.getIntArray(0),"00000000000000");
//               	 System.out.println(currentTask.getStringArray(0));
//              	 System.out.println(currentTask.getIntArray(0));
               	 break;
                  case TASK_DES_FEISTEL:
                	  
                 	 solution=feistefunction(currentTask.getStringArray(0), currentTask.getStringArray(1));
                 	 
//                 	 System.out.println(currentTask.getStringArray(0));
//                  	 System.out.println(currentTask.getStringArray(1));
                 	 break;
                	 
                 
                  case TASK_DES_ROUND:
                	  
                 	 solution= calculatround(currentTask.getStringArray(0), currentTask.getStringArray(1), currentTask.getStringArray(2), currentTask.getIntArray(0));
                      
                 	 break;
                	 
              
              case TASK_AES_GF8:
            	   solution =bintohex(wastobin(regetgf8(gf8(currentTask.getStringArray(0), currentTask.getStringArray(1)))));
               break;
              case TASK_AES_MIXCOLUMNS:
            	  solution = mixcolumns(currentTask.getStringArray(0));
            	  break;
              case TASK_AES_TRANSFORMATION:
            	  solution= mixcolumns(shiftrowsblock(subbytes(currentTask.getStringArray(0))));
            	  break;
              
//              case TASK_AES_KEYEXPANSION:
//            	  solution= aesKeyExpansion(currentTask.getStringArray(0));
//            	  break;
         
             case TASK_RC4_LOOP:
            	 solution = GenRc4Loop(currentTask.getStringArray(0), currentTask.getStringArray(1));
            	
            	 break;
             case TASK_RC4_KEYSCHEDULE:
            	 solution= KeySchedule(currentTask.getStringArray(0)) ;
            	 break;
             case   TASK_RC4_ENCRYPTION:
            	 solution = encryptrc4(currentTask.getStringArray(0), currentTask.getStringArray(1));
            	 break;
             case TASK_DIFFIEHELLMAN:
            	 solution=pubkeymore(currentTask.getIntArray(0), currentTask.getIntArray(1), currentTask.getDoubleArray(0),con,currentTask);
          break;
             case TASK_RSA_ENCRYPTION:
           	  solution=   rsaencrypt(currentTask.getIntArray(0), currentTask.getIntArray(1), currentTask.getStringArray(0));
           	  break;
           

            }
            
            if (con.sendSolution(solution))
            	
       	
                System.out.println("Aufgabe " + task + ": Lösung korrekt"+ solution);
            else
                System.out.println("Aufgabe " + task + ": Lösung falsch"+ solution);
           
            
        }
    }

   

	public static void main(String[] args) {
        Client c = new Client();
        if (c.con.isReady())
            c.taskLoop();
          c.con.close();
        
       
    }
	public static String rsadecrypt(String a, String b,TaskObject Task) {
		return null;
		
	}
	
	public static String pubkeymore(int p,int g, double b,Connection con, TaskObject Task) {
	double z =0;
		String[] t = { z+""};
		con.sendMoreParams(Task, t);
		return null;
		
	}
	
	
	
	
	
	
	
	

    
   
    
   /////////////////// ///////////////////////////////schon gel�st///////////////////////////////////////
    //aufgabe 3
    // modulo berechnung

	 public static String  modulofac(int a, int b) {
		 
		 try {
         if(a-b >b) {
             return modulofac(a-b,b);
         }else {
             return String.valueOf(a-b) ;
         }
		 }catch (StackOverflowError e) {
			// TODO: handle exception
			 return"error";
		}
 
     }

public static String faktorisierung (int number) {

   String resultstr= "";

   
   if(primzahl(number)==0){
	   
	   resultstr =String.valueOf(number);
	   
   }else if(primzahl(number)!=1){
	   
	   resultstr =String.valueOf(primzahl(number));
	   
	   int number2= number /primzahl(number);
	   while(number2>0) {
		   
		   if(primzahl(number2)==1) {
			   
			   resultstr += "*"+String.valueOf(number2);
		   }else {
		   resultstr += "*"+String.valueOf(primzahl(number2));
		   }
		   
		   
		   if(primzahl(number2)==1) {
			  number2 =0; 
		   }else {
		   number2= number2 / primzahl(number2);
		   }
	   }
	   
   }

   return resultstr;
}

// hilfe fuer aufgabe 4
public static int primzahl(int number) {

int result =1;
int count=number;
while(count>2) {
	if(modulofac(number,count-1).equals( String.valueOf(count-1) ) ){
        result = count-1;
	} 

	count--;
    }
    


return result;

}


	
	public static String modulo(int number, int modulo) {
		while(number >= modulo) {
			number -= modulo;
		}
		while(number < 0) {
			number += modulo;
		}
		return String.valueOf(number);
		
	}

    // aufgabe 4 faktorisierung
//   s
    public static String xor( String res1,String res2){

        String result="";
        
        String a= hextobin(res1);
        String b =hextobin(res2);
        
        if(res1.length()<res2.length()) {
       	 a= hextobin(res2);
       	 b= hextobin(res1);
        }
        int different = a.length()-b.length();
        for(int i=0;i<different;i++) {
       	 
        result +=a.charAt(i);
       	 
        }
       
        for(int i=0;i<b.length();i++) {
       	 if(b.charAt(i)==a.charAt(different+i)) {
       		 
       		 result+='0';
       	 }else {
       		 result+='1';
       	 }
       	 
        }
     
           return result;
       }
    
 public static String hextobin(String hex) {
   
        int count = 0;
  String result="";
  
	
  
  
         while(count<hex.length()) {

        	 
        	 if(hex.charAt(count)=='0') {
                 result +="0000";
             }else if(hex.charAt(count)=='1') {
                 result +="0001";
             }else if(hex.charAt(count)=='2') {
                 result +="0010";
             }else if(hex.charAt(count)=='3') {
                 result +="0011";
             }else if(hex.charAt(count)=='4') {
                 result +="0100";
             }else if(hex.charAt(count)=='5') {
                 result +="0101";
             }else if(hex.charAt(count)=='6') {
                 result +="0110";
             }else if(hex.charAt(count)=='7') {
                 result +="0111";
             }else if(hex.charAt(count)=='8') {
                 result +="1000";
             }else if(hex.charAt(count)=='9') {
                 result +="1001";
             }else if(hex.charAt(count)=='a'||hex.charAt(count)=='A') {
                 result +="1010";
             }else if(hex.charAt(count)=='b'||hex.charAt(count)=='B') {
                 result +="1011";
             }else if(hex.charAt(count)=='c'||hex.charAt(count)=='C') {
                 result +="1100";
             }else if(hex.charAt(count)=='d'||hex.charAt(count)=='D') {
                 result +="1101";
             }else if(hex.charAt(count)=='e'||hex.charAt(count)=='E') {
                 result +="1110";
             }else if(hex.charAt(count)=='f'||hex.charAt(count)=='F') {
                 result +="1111";
            }

            
        
        	 count++;
         }
        
        return result;
 }
 
 
 ////vignere
 
 
 public static String vignere(String ciphertext , String key) {
     String[] []vig = new String[][] {
             {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"},
             {"B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A"},
             {"C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B"},
             {"D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C"},
             {"E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D"},
             {"F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E"},
             {"G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F"},
             {"H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G"},
             {"I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H"},
             {"J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I"},
             {"K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J"},
             {"L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K"},
             {"M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L"},
             {"N","O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M"},
             {"O","P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N"},
             {"P","Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"},
             {"Q","R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"},
             {"R","S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q"},
             {"S","T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R"},
             {"T","U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S"},
             {"U","V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T"},
             {"V","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U"},
             {"W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V"},
             {"X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W"},
             {"Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X"},
             {"Z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y"}};




             String cipherupper= transformlowtoupper(ciphertext);
             String keyupper = transformlowtoupper(key);



     char[] cipherarray =  cipherupper.toCharArray();
     char[] keyarray =  keyupper.toCharArray();
    // char []resultchar =cipherarray ;
     String result="";


     for(int i=0; i<keyarray.length; i++) {
     int	count =i;
            while(count<cipherarray.length) {
         	   
            
         for(int j=0; j<vig.length; j++) {

             if(vig[j][0].charAt(0)== keyarray[i]) {

             			                	
                 for(int l=0; l<vig.length; l++) {
                 	
                 
                     if(vig[j][l].charAt(0)== cipherarray[count]) {

                         result +=vig[0][l].charAt(0);
                  break;
                     }
                 }
             }

         }

break;
     }
     }
     return  result;

 }

 public static String transformlowtoupper(String text) {

		String result ="";
		for(int i=0;i<text.length();i++) {

		if(text.charAt(i)=='a'||text.charAt(i)=='A') {
		result+='A';
		}else if(text.charAt(i)=='b'||text.charAt(i)=='B') {
		result+='B';
		}else if(text.charAt(i)=='c'||text.charAt(i)=='C') {
		result+='C';
		}else if(text.charAt(i)=='d'||text.charAt(i)=='D') {
		result+='D';
		}else if(text.charAt(i)=='e'||text.charAt(i)=='E') {
		result+='E';
		}else if(text.charAt(i)=='f'||text.charAt(i)=='F') {
		result+='F';
		}else if(text.charAt(i)=='g'||text.charAt(i)=='G') {
		result+='G';
		}else if(text.charAt(i)=='h'||text.charAt(i)=='H') {
		result+='H';
		}else if(text.charAt(i)=='i'||text.charAt(i)=='I') {
		result+='I';
		}else if(text.charAt(i)=='j'||text.charAt(i)=='J') {
		result+='J';
		}else if(text.charAt(i)=='k'||text.charAt(i)=='K') {
		result+='K';
		}else if(text.charAt(i)=='l'||text.charAt(i)=='L') {
		result+='L';
		}else if(text.charAt(i)=='m'||text.charAt(i)=='M') {
		result+='M';
		}else if(text.charAt(i)=='n'||text.charAt(i)=='N') {
		result+='N';
		}else if(text.charAt(i)=='o'||text.charAt(i)=='O') {
		result+='O';
		}else if(text.charAt(i)=='p'||text.charAt(i)=='P') {
		result+='P';
		}else if(text.charAt(i)=='q'||text.charAt(i)=='Q') {
		result+='Q';
		}else if(text.charAt(i)=='r'||text.charAt(i)=='R') {
		result+='R';
		}else if(text.charAt(i)=='s'||text.charAt(i)=='S') {
		result+='S';
		}else if(text.charAt(i)=='t'||text.charAt(i)=='T') {
		result+='T';
		}else if(text.charAt(i)=='u'||text.charAt(i)=='U') {
		result+='U';
		}else if(text.charAt(i)=='v'||text.charAt(i)=='V') {
		result+='V';
		}else if(text.charAt(i)=='w'||text.charAt(i)=='W') {
		result+='W';
		}else if(text.charAt(i)=='x'||text.charAt(i)=='X') {
		result+='X';
		}else if(text.charAt(i)=='y'||text.charAt(i)=='Y') {
		result+='Y';
		}else if(text.charAt(i)=='z'||text.charAt(i)=='Z') {
		result+='Z';
		}else if(text.charAt(i)=='0') {
			result+='0';
		}else if(text.charAt(i)=='1') {
			result+='1';
		}else if(text.charAt(i)=='2') {
			result+='2';
		}else if(text.charAt(i)=='3') {
			result+='3';
		}else if(text.charAt(i)=='4') {
			result+='4';
		}else if(text.charAt(i)=='5') {
			result+='5';
		}else if(text.charAt(i)=='6') {
			result+='6';
		}else if(text.charAt(i)=='7') {
			result+='7';
		}else if(text.charAt(i)=='8') {
			result+='8';
		}else if(text.charAt(i)=='9') {
			result+='9';
		}

		}
		return result;
		}

 
 

 
 
 ///aufgabe 10 Multiplication

public static int countnotnull(int[]array) {
	
	int result=0;
	
	for(int i =0; i<array.length;i++) {
		if(array[i]!=0 ) {
			result+=1;
			
	}
	
	}
	
	return result;
}
public static String bintohex(String str) {
	String result ="";
	String str1="";
	String str2="";
	if(str.length()>4) {
	int limit= str.length()-4;
	
	for(int i=0;i<limit;i++) {
		str1+=str.charAt(i);
		
	}
	for(int i=limit;i<str.length();i++) {
		str2+=str.charAt(i);
		
		
	}
	
	
	String str11= bintohexone(str1);
	String str22= bintohexone(str2);

	result+=str11;
	result+=str22;
}else {
	result= bintohexone(str);
}
	
	return result;
	
}



public static String bintohexone(String str) {
	
	String result ="";
	
if (str.equals("0000")||str.equals("000")||str.equals("00")||str.equals("0")) {
result= "0";
}else if (str.equals("0001")||str.equals("001")||str.equals("01")||str.equals("1")) {
result= "1";
}else if (str.equals("0010")||str.equals("010")||str.equals("10")) {
result= "2";
}else if (str.equals("0011")||str.equals("011")||str.equals("11")) {
result= "3";
}else if (str.equals("0100")||str.equals("100")) {
result= "4";
}else if (str.equals("0101")||str.equals("101")) {
result= "5";
}else if (str.equals("0110")||str.equals("110")) {
result= "6";
}else if (str.equals("0111")||str.equals("111")) {
result= "7";
}else if (str.equals("1000")) {
result= "8";
}else if (str.equals("1001")) {
result= "9";
}else if (str.equals("1010")) {
result= "A";
}else if (str.equals("1011")) {
result= "B";
}else if (str.equals("1100")) {
result= "C";
}else if (str.equals("1101")) {
result= "D";
}else if (str.equals("1110")) {
result= "E";
}else if (str.equals("1111")) {
result= "F";
}else if (str=="") {
result= "";
}
	return result;
	


	
	
	
}




 public static String wastobin(int[]array) {
	 int result=0;
	for(int i=0;i<array.length;i++) {
		if(array[i]==7) {
			result+=10000000;
		}else if(array[i]==6) {
			result+=1000000;
		}else if(array[i]==5) {
			result+=100000;
		}else if(array[i]==4) {
			result+=10000;
		}else if(array[i]==3) {
			result+=1000;
		}else if(array[i]==2) {
			result+=100;
		}else if(array[i]==1) {
			result+=10;
		}else if(array[i]==0) {
			result+=1;
		}
	}
	 
	String resultstr = String.valueOf(result);
	return resultstr;
}



 
public static int getresultgf8(int[]array) {
	

int result=0;
for(int i=0;i<array.length;i++) {
	result+=power(2, array[i]);
}
	
	return result;
}


public static int[]getinorder(int[]array){
	 int result = 0; 
	 
	 for (int i = 0; i < array.length; i++) {     
            for (int j = i+1; j < array.length; j++) {     
               if(array[i] < array[j]) {    
                   result = array[i];    
                   array[i] = array[j];    
                   array[j] = result;    
               }     
            }     
        }    
	return array;
}





public static int getcountduplicate(int[]array) {
	int result = 0;
	int count=0;
	for(int i=0;i<array.length;i++) {
		for(int j=0;j<array.length;j++) {
		
			if(array[j]==array[i]) {
				count+=1;
			}
			
		}
		if(count>=2) {
			result+=1;
		}
		count=0;
	}
	
	return result;
}



// gibt die position if char ungleich 0 und l�scht die 0
public static int [] getposition(String bin) {
	
	int []result= new int[bin.length()];
	int count = bin.length();
	
	int countarray=0;
	while( countarray<bin.length()) {
		

		if(bin.charAt(countarray)=='1') {
			result[countarray]=count;
			
			
		}else if(bin.charAt(countarray)=='0') {
			result[countarray]=0;

		}

		countarray++;
		count--;
		
	}
	
		int []resultdef = new int[countnotnull(result)];
	

		int count1 =0 ;
		while (count1<resultdef.length) {
      
	for(int i =0; i<result.length;i++) {
		
		if(result[i]!=0) {
			
		resultdef[count1]= result[i];
		count1++;
		}
	}
	
	
      }
	
	return resultdef;
}


public static int[] gf8(String hexa1,String hexa2) {
	
	String bin1 =hextobin(hexa1);
	String bin2 =hextobin(hexa2);
	int[] positionbin1= getposition(bin1);
	int[] positionbin2= getposition(bin2);
	
//	int count1 = 0;
//	int count2 =0;
	
	int []cache = new int[positionbin2.length*positionbin1.length];
	int count3 =0;
	while(count3 <cache.length) {
	for(int i=0;i<positionbin1.length;i++) {
		
		for(int j=0;j<positionbin2.length;j++) {
			
			cache[count3]= positionbin1[i]+ positionbin2[j]-1;
			count3++;
		}
	}
	
	
	}
	
	int []cachewithouduplicate =new int[cache.length-getcountduplicate(cache)];
	int count=0;
	int a=0;
	
		
	for(int i=0;i<cache.length;i++) {
		for(int j=0;j<cache.length;j++) {
		  if(cache[i]==cache[j]) {
			a+=1;
		}
			
		}
		if(a==1) {
			cachewithouduplicate[count]= cache[i];
			count+=1;
		}
		a=0;
		
	}
	
	for(int i =0;i<cachewithouduplicate.length;i++) {
		cachewithouduplicate[i]-=1;
	}
	

		return cachewithouduplicate;
	} 	



public static int[] getgf8(int[]cache) {
	int[]notnull= new int[] {8,4,3,1,0};
	cache = getinorder(cache);
	
	//while(cache[0]>7) {
		int diff =cache[0]-notnull[0];
	int[]notnulldiff= new int[] {8+diff,4+diff,3+diff,1+diff,0+diff};
	
	int[]resultcache= new int[cache.length+notnulldiff.length];
	for(int i=0;i< notnulldiff.length;i++) {
		resultcache[i]=notnulldiff[i];
	}
	for(int i=5;i< cache.length+5;i++) {
		resultcache[i]=cache[i-5];
	}
	
	
	int []recachewithouduplicate =new int[resultcache.length-getcountduplicate(resultcache)];
	int count=0;
	int a=0;
	
		
	for(int i=0;i<resultcache.length;i++) {
		for(int j=0;j<resultcache.length;j++) {
		  if(resultcache[i]==resultcache[j]) {
			a+=1;
		}
			
		}
		if(a==1) {
			recachewithouduplicate[count]= resultcache[i];
			count+=1;
		}
		a=0;
		
	}
	
	return getinorder(recachewithouduplicate);
	//}else {
		//return cache;
	//}
}

public static int[] regetgf8(int[]cache) {
	//cache= getinorder(getgf8(cache));
	if(cache.length>0) {
	if(cache[0]>7) {
	return	regetgf8(getgf8(cache));
	}else {
	
	return cache;
	}
}else {
	return cache;
}
}
 
 ///Aufgabe mixculumn

public static String executemixcol(String str, String cx) {
	str =transformlowtoupper(str);
	//cx=transformlowtoupper(cx);
	
	
	String result="";
	String cache1 ="";
	String cache2 ="";
	String cache3 ="";
	String cache4 ="";
	
	String cache11 ="";
	String cache12 ="";
	String cache13 ="";
	String cache14 ="";
	
	String c1="";
	String c2="";
	String c3="";
	String c4="";
	
	c1+=cx.charAt(0);
	c2+=cx.charAt(1);
	c3+=cx.charAt(2);
	c4+=cx.charAt(3);
	
	for(int i =0;i<str.length();i++) {
		if(i<2) {
			cache1+=str.charAt(i);			
		}
		else if(1<i && i<4) {
			cache2+=str.charAt(i);			
		}
		else if(3<i && i<6) {
			cache3+=str.charAt(i);	
			
		}else if(5<i && i<8) {
			cache4+=str.charAt(i);			
		}
	}
	
cache11=transformlowtoupper( bintohex(wastobin (regetgf8( gf8(transformlowtoupper(cache1), c1)))));
cache12=transformlowtoupper( bintohex(wastobin (regetgf8( gf8(transformlowtoupper(cache2), c2)))));	
cache13=transformlowtoupper( bintohex(wastobin (regetgf8( gf8(transformlowtoupper(cache3), c3)))));	
cache14= transformlowtoupper(bintohex(wastobin (regetgf8( gf8(transformlowtoupper(cache4), c4)))));


 result = bintohex(  xor(cache11, cache12));
result = bintohex( xor(result, cache13));
result = bintohex(  xor(result, cache14));

	return result;
}
	
	
	
	
	public static String mixcolumns(String str) {
		str =transformlowtoupper(str);
		String cx1="2311";
		String cx2="1231";
		String cx3="1123";
		String cx4="3112";
		String result ="";
		
		//gibt zwei erste char
		String cache1 ="";
		String cache2 ="";
		String cache3 ="";
		String cache4 ="";
		
		for(int i =0;i<str.length();i++) {
			if(i<8) {
				cache1+=str.charAt(i);			
			}
			if(7<i && i<16) {
				cache2+=str.charAt(i);			
			}
			if(15<i && i<24) {
				cache3+=str.charAt(i);	
				
			}if(23<i && i<32) {
				cache4+=str.charAt(i);			
			}
		}
		
		result+=executemixcol(cache1, cx1);
		result+=executemixcol(cache1, cx2);
		result+=executemixcol(cache1, cx3);
		result+=executemixcol(cache1, cx4);
		
		result+=executemixcol(cache2, cx1);
		result+=executemixcol(cache2, cx2);
		result+=executemixcol(cache2, cx3);
		result+=executemixcol(cache2, cx4);
		
		result+=executemixcol(cache3, cx1);
		result+=executemixcol(cache3, cx2);
		result+=executemixcol(cache3, cx3);
		result+=executemixcol(cache3, cx4);
		
		result+=executemixcol(cache4, cx1);
		result+=executemixcol(cache4, cx2);
		result+=executemixcol(cache4, cx3);
		result+=executemixcol(cache4, cx4);
		
		return result;
		
	}
 
 
 
 
 
	
public static String getfromsbox(String str1) {
		
		String str = transformlowtoupper(str1);
		
		String result= "a";
		
		
		String [][]sbox = new String [][] {
			{"n", "0","1",  "2","3", "4",  "5", "6", "7","8",  "9", "A", "B", "C","D", "E",  "F"},
			{"0","63","7c","77","7b","f2","6b","6f","c5","30","01","67","2b","fe","d7","ab","76"},
			{"1","ca","82","c9","7d","fa","59","47","f0","ad","d4","a2","af","9c","a4","72","c0"},
			{"2","b7","fd","93","26","36","3f","f7","cc","34","a5","e5","f1","71","d8","31","15"},
			{"3","04","c7","23","c3","18","96","05","9a","07","12","80","e2","eb","27","b2","75"},
			{"4","09","83","2c","1a","1b","6e","5a","a0","52","3b","d6","b3","29","e3","2f","84"},
			{"5","53","d1","00","ed","20","fc","b1","5b","6a","cb","be","39","4a","4c","58","cf"},
			{"6","d0","ef","aa","fb","43","4d","33","85","45","f9","02","7f","50","3c","9f","a8"},
			{"7","51","a3","40","8f","92","9d","38","f5","bc","b6","da","21","10","ff","f3","d2"},
			{"8","cd","0c","13","ec","5f","97","44","17","c4","a7","7e","3d","64","5d","19","73"},
			{"9","60","81","4f","dc","22","2a","90","88","46","ee","b8","14","de","5e","0b","db"},
			{"A","e0","32","3a","0a","49","06","24","5c","c2","d3","ac","62","91","95","e4","79"},
			{"B","e7","c8","37","6d","8d","d5","4e","a9","6c","56","f4","ea","65","7a","ae","08"},
			{"C","ba","78","25","2e","1c","a6","b4","c6","e8","dd","74","1f","4b","bd","8b","8a"},
			{"D","70","3e","b5","66","48","03","f6","0e","61","35","57","b9","86","c1","1d","9e"},
			{"E","e1","f8","98","11","69","d9","8e","94","9b","1e","87","e9","ce","55","28","df"},
			{"F","8c","a1","89","0d","bf","e6","42","68","41","99","2d","0f","bo","54","bb","16"}
		};
		
		
		for(int i = 0; i<sbox.length;i++) {
			 
			if(sbox[i][0].charAt(0)==str.charAt(0)) {
				
					for(int j= 0; j<sbox[0].length;j++) {
					if(sbox[0][j].charAt(0)==str.charAt(1)) {
						
						result= sbox[i][j];
						
						
					}
				}
			}
			
		}
		
		return result;
		
		
	}
	
	
	
	
public static String subbytes(String str){
	String result= "";
	String cache="";
	

int	count =0;
	while(count <str.length()) {

		String b ="";
		b +=str.charAt(count);
		b +=str.charAt(count+1);
		cache = getfromsbox(b);
		result +=cache;
		
		count+=2;
	}
	
	return result;
	
	
}
	
	
public static String  shiftrowsblock(String str) {
	
String str1="";	String str2=""; String str3="";String str4="";String str5="";String str6="";
String str7="";String str8="";String str9="";String str10="";String str11="";String str12="";
String str13="";String str14="";String str15="";String str16="";

 str1 +=str.charAt(0);     
 str1 +=str.charAt(1);
 
 str2 +=str.charAt(2);
 str2 +=str.charAt(3);
 
 str3 +=str.charAt(4);
 str3 +=str.charAt(5);
 
 str4 +=str.charAt(6);     
 str4 +=str.charAt(7);
 
 str5 +=str.charAt(8);
 str5 +=str.charAt(9);
 
 str6 +=str.charAt(10);
 str6 +=str.charAt(11);
 
 str7 +=str.charAt(12);     
 str7 +=str.charAt(13);
 
 str8 +=str.charAt(14);
 str8 +=str.charAt(15);
 
 str9 +=str.charAt(16);
 str9 +=str.charAt(17);
 
 str10 +=str.charAt(18);     
 str10 +=str.charAt(19);
 
 str11 +=str.charAt(20);
 str11 +=str.charAt(21);
 
 str12 +=str.charAt(22);
 str12 +=str.charAt(23);
 
 str13 +=str.charAt(24);     
 str13 +=str.charAt(25);
 
 str14 +=str.charAt(26);
 str14 +=str.charAt(27);
 
 str15 +=str.charAt(28);
 str15 +=str.charAt(29);
 
 
 str16 +=str.charAt(30);
 str16 +=str.charAt(31);
 
	String result ="";
	
	result+= str1;
	result+= str6;
	result+= str11;
	result+= str16;
	
	result+= str5;
	result+= str10;
	result+= str15;
	result+= str4;
	
	result+= str9;
	result+= str14;
	result+= str3;
	result+= str8;
	
	result+= str13;
	result+= str2;
	result+= str7;
	result+= str12;
	
	return result;
	
}
	
	

 
 
// aufgabe deskeygen
public static int[] getcblock(String str) {
	
	int [] cblock={
	   		57, 49, 41, 33, 25, 17, 9,
	   		1, 58, 50, 42, 34, 26, 18,
	   		10, 2, 59, 51, 43, 35, 27,
	   		19, 11, 3, 60, 52, 44, 36

	   	};
	int []result = new int[cblock.length];
	for(int i=0;i<cblock.length;i++) {
	
		 result[i]= chartointeger( str.charAt(cblock[i]-1));
				
	
	}

	return result;
	
}

public static  int[] getdblock(String str) {
	
	int [] dblock={
	   		63, 55, 47, 39, 31, 23, 15,
	   		7, 62, 54, 46, 38, 30, 22, 
	   		14, 6, 61, 53, 45, 37, 29,
	   		21, 13, 5, 28, 20, 12, 4
	   	};
	int []result = new int[dblock.length];
	for(int i=0;i<dblock.length;i++) {
		 result[i]= chartointeger(str.charAt(dblock[i]-1));
			
		}

	return result;
	
	
}


public static int chartointeger(char a){
	int result = 0;
	 if (a =='1') {result = 1;}
	else if (a =='0') {result = 0;}
	else if (a =='2') {result = 2;}
	else if (a =='3') {result = 3;}
	else if (a =='4') {result = 4;}
	else if (a =='5') {result = 5;}
	else if (a =='6') {result = 6;}
	else if (a =='7') {result = 7;}
	else if (a =='8') {result = 8;}
	else if (a =='9') {result = 9;}
	
	return result;
}

public static int getleftschift(int round) {
	int result =0;
switch (round) {
	case 1:result =1;break;
	case 2:result =2;break;
	case 3:result =4;break;
	case 4:result =6;break;
	case 5:result =8;break;
	case 6:result =10;break;
	case 7:result =12;break;
	case 8:result =14;break;
	case 9:result =15;break;
	case 10:result =17;break;
	case 11: result =19;break;
	case 12:result =21;break;
	case 13:result =23;break;
	case 14:result =25;break;
	case 15: result =27;break;
	case 16: result =28;break;
	default:
		break;
	}
	
	return result;
}



	public static String DesKeySchedule (String key, int round){
		
		String result = "";
		int[] c = new int[28];
		int[] d = new int[28];

		c= getcblock(key);
	  	d=getdblock(key);
		

		int[] cleftshift = new int[28];
		int[] dleftshift = new int[28];
		cleftshift = shiftLeft(c, getleftschift(round));
		
		dleftshift = shiftLeft(d, getleftschift(round));
	
		int[] cahe = new int[56];
		for (int i = 0; i < c.length; i++){
			
			cahe[i] = cleftshift[i];
		}
		
		for (int i = 28; i < 56; i++){
			cahe[i] = dleftshift[i-28];
			
		}

		result = getpc2block(cahe);

		
		return result;
	}
	

	public  static String getpc2block(int[] cahe) {
		String result ="";
	 int[] pc2 = {
		   		14, 17, 11, 24, 1, 5, 
		   		3, 28, 15, 6, 21, 10,
		   		23, 19, 12, 4, 26, 8,
		   		16, 7, 27, 20, 13, 2,
		   		41, 52, 31, 37, 47, 55,
		   		30, 40, 51, 45, 33, 48,
		   		44, 49, 39, 56, 34, 53,
		   		46, 42, 50, 36, 29, 32
		   	};
	 for(int i=0;i<pc2.length;i++) {
		
	  result += cahe[pc2[i]-1];
			}

	 return result;
	}
	
		public static int moduloint(int number, int modulo) {
			while(number >= modulo) {
				number -= modulo;
		}
			
			while(number < 0) {
				number += modulo;
			}
			return number;
		}
		public static int [] shiftLeft(int [] s, int shift) {
			int [] result = new int [s.length];
			shift = moduloint(shift, s.length);
			int [] cache1 = new int [s.length-shift];
			int [] cache2 = new int [shift];
			
			for (int i =0; i<cache2.length;i++) {
				
					cache2[i]=s[i];
			}
			for (int i =0; i<cache1.length;i++) {
					 
					cache1[i]=s[i+shift];
			}
			for (int i =0; i<cache1.length;i++) {
				result[i]= cache1[i];
			}
			
			for (int i =0; i<cache2.length;i++) {
				result[i-shift+s.length]= cache2[i];
			}
			return result;

		}
		
		

    
    // generation loop rc4
    
		

		

	public static String GenRc4Loop(String state, String cleartext){
		int[] result = new int[cleartext.length()];
		int[] statearray = movetrich(state);
		
		int j = 0;
		int i = 0;
		for (int k = 0;  k  < cleartext.length();  k++){
			i = moduloint(i + 1 , statearray.length);
			j = moduloint(j + statearray[i], statearray.length);
			
			statearray= Swaping(i, j, statearray);
			int	b = moduloint(statearray[i] + statearray[j], statearray.length);
			
			result[k] = statearray[b];
		}
		
		String resultstr = "";
		for (int l = 0; l < result.length; l++){
			
			resultstr += (result[l]);
		}
		
		return resultstr;
	}


	public static int[] Swaping(int a, int b, int[] statearray){
		
		int[] result = new int[] {};
		 result = statearray;
		
		
		int c = statearray[b];
		result[b] = result[a];
		result[a] = c;
		return result;
	}
	
	
	
	
	
public static  int []movetrich(String str) {
		
		String []result =new String[str.length()];
		String cache="";
		for(int i=0;i<str.length();i++) {
			 
			if(str.charAt(i)=='_') {
				result[i]=cache;
				cache="";

				
			}else {
				cache+=str.charAt(i);
			}
			
		}
		result[result.length-1]=cache;
		
		int []resultnotnull =new int[getcounter(result)];
		int count=0;
		for(int i=0;i<result.length;i++) {
			if(result[i]!=null) {
				resultnotnull[count]=Integer.valueOf(result[i]);
				count++;
			}
		}
		
		return resultnotnull;
	}
	
	public static int getcounter(String []str) {
		int result =0;
		for(int i=0;i<str.length;i++) {
			if(str[i]!=null) {
				result+=1;
				
			}
		}
		
		return result;
	}
    
    
    
	

	
	public static String KeySchedule(String Key){
		String result = "";
		
		int[] KeyInt_out = movetrich(Key);
		
		int[] state = new int[KeyInt_out.length];
		int count = 0;
		while ( count < KeyInt_out.length){
			state[count] = count;
			count++;
		}	state = calculate(KeyInt_out, state);
		
		for (int i = 0; i < state.length - 1; i++){
			
			result += String.valueOf(state[i]);
			 result += "_";
		}
		result += String.valueOf(state[state.length - 1]);
		
		return result;
	}	
	
	public static int[]calculate(int []KeyInt_out,int []state){
		int j = 0;
		for (int i = 0; i < KeyInt_out.length; i++){
			
			j = j + state[i] +KeyInt_out[i];
			
			j = moduloint(j, KeyInt_out.length);
			
			state = Swaping (i, j, state);
		}
		return state;
		
		
	}
	
	
	
	
	
	 public static String xorbin( String res1,String res2){

	        String result="";
	        
	        String a= (res1);
	        String b =(res2);
	        
	        if(res1.length()<res2.length()) {
	       	 a=(res2);
	       	 b= (res1);
	        }
	        
	        int different = a.length()-b.length();
	        for(int i=0;i<different;i++) {
	       	 
	        result +=a.charAt(i);
	       	 
	        }
	       
	        for(int i=0;i<b.length();i++) {
	       	 if(b.charAt(i)==a.charAt(different+i)) {
	       		 
	       		 result+='0';
	       	 }else {
	       		 result+='1';
	       	 }
	       	 
	        }
	     
	           return result;
	       }
	    
	
	public  static String getepansion(String str) {
		String result ="";
		int [] expvalue={32,1,2,3,4,5,4,5,6,7,8,9,8,9,10,11,
				12,13,12,13,14,15,16,17,16,17,18,19,20,21,
				20,21,22,23,24,25,24,25,26,27,28,29,28,29,30,31,32,1};
	 for(int i=0;i<expvalue.length;i++) {
		
	  result += str.charAt(expvalue[i]-1);
			}
	
	 return result;
	}
	
	public static String finpermutation(String str) {
		
		String result="";
		
		int [] p={16,7,20,21,29,12,28,17,1,15,23,26,5,18,31,
				10,2,8,24,14,32,27,3,9,19,13,30,6,22,11,4,25};
		 for(int i=0;i<p.length;i++) {
			 try {
			  result += str.charAt(p[i]-1);
			 }catch(StringIndexOutOfBoundsException e) {
		   		 // System.out.println("array not 32");
		   	 }
					}
			 
		return result;
		
	}
	
	public static String feistefunction(String str, String key) {
		
		String rblock = "";
		rblock = str.substring(32);
		
		String lblock = "";
		lblock = str.substring(0, 32);
		
		String expansion = "";
		expansion= getepansion(rblock);
		
		String gexort="";
		
		gexort= xorbin(expansion, key);
		
		String calsbox = calculatesboxcomplete(gexort);
		
	  String cache =finpermutation(calsbox); 
		String result = xorbin(cache, lblock);
		
		return result;
		
		
	} 
	 public static String getasciiixor(List <String>ascii ,int []cache){ 
	    	
	    	String result = "";
	    	String asci = "";
	    	for (int i = 0; i < ascii.size(); i++) {
				asci += ascii.get(i);
			}
	    	
	    	int count =7;
	    	List <Integer> code = new ArrayList<>();
	    	for (int i = 0; i <8; i++) {
	    		code.add(0);
			}
	
	    	for (int i = 0; i < cache.length; i++) {
	    		
	    		for (int j = 0; j < count; j++) {
				
	    			code.set(j, moduloint(cache[i], 2));
	    			cache[i]/=2;
				}
	    		int count1=code.size()-1;
	    		while (count1>=0) {
					result += String.valueOf(code.get(count1));
					count1--;
				}

			}
	    	
	    	
	    	
			return xorbin(result, asci);
	    	
	    }
	public static String [][] getsbox(int box) {
		String[][] result = null;
		String [][] sBox1={
				{"23","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"},
				{"00",  "14",   "4",  "13",   "1",   "2",  "15",  "11",   "8",   "3",  "10",   "6",  "12",   "5",   "9",   "0",   "7"},
				{"01",   "0",  "15",   "7",   "4",  "14",   "2",  "13",   "1",  "10",   "6",  "12",  "11",   "9",   "5",   "3",   "8"},
				{"10",   "4",   "1",  "14",   "8"  ,"13",   "6"   ,"2",  "11",  "15",  "12",   "9",   "7",   "3",  "10",   "5",   "0"},
				{"11",  "15",  "12",   "8",   "2",   "4",   "9",   "1",   "7",   "5",  "11",   "3",  "14",  "10",   "0",   "6",  "13"}
				};
		
		String[][] sBox2 = {
				{"23","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"},
				{"00",  "15",   "1",   "8",  "14",   "6",  "11",   "3",   "4",   "9",   "7",   "2",  "13",  "12",   "0",   "5",  "10" },
				{"01",   "3",  "13",   "4",   "7",  "15",   "2",   "8",  "14",  "12",   "0",   "1",  "10",   "6",   "9",  "11",   "5" },
				{"10",   "0",  "14",   "7",  "11",  "10",   "4",  "13",   "1",   "5",   "8",  "12",   "6",   "9",   "3",   "2",  "15" },
				{"11",  "13",   "8",  "10",   "1",   "3",  "15",   "4",   "2",  "11",   "6",   "7",  "12",   "0",   "5",  "14",   "9" } };
		
		String[][] sBox3 = {
				{"23","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"},
				{"00", "10", "0", "9", "14", "6", "3", "15", "5", "1", "13", "12", "7", "11", "4","2", "8" },
				{"01", "13", "7", "0", "9", "3", "4", "6", "10", "2", "8", "5", "14", "12", "11", "15", "1" },
				{"10", "13", "6", "4", "9", "8", "15", "3", "0", "11","1", "2", "12", "5", "10", "14", "7" },
				{"11", "1", "10", "13", "0", "6", "9", "8", "7","4", "15", "14", "3", "11", "5", "2", "12" } };
		
		String[][] sBox4 = {
				{"23","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"},
				{"00",   "7",  "13",  "14",   "3",   "0", "6", "9", "10", "1", "2", "8", "5", "11", "12", "4", "15" },
				{"01",  "13",   "8",  "11",   "5",   "6", "15", "0", "3", "4", "7", "2", "12", "1", "10", "14", "9" },
				{"10",  "10",   "6",   "9",   "0",  "12", "11", "7", "13", "15", "1", "3", "14", "5", "2", "8", "4" },
				{ "11",  "3",  "15",   "0",   "6",  "10", "1", "13", "8", "9", "4", "5", "11", "12", "7", "2", "14" } };
		
		String[][] sBox5 = { 
				{"23","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"},
				{"00",   "2", "12", "4", "1", "7", "10", "11", "6", "8", "5", "3", "15", "13", "0", "14", "9" },
				{"01", "14", "11", "2", "12", "4", "7", "13", "1", "5", "0", "15", "10", "3","9", "8", "6" },
				{"10", "4", "2", "1", "11", "10", "13", "7", "8", "15", "9", "12", "5", "6", "3", "0", "14" },
				{"11", "11", "8", "12","7", "1", "14", "2", "13", "6", "15", "0", "9", "10", "4", "5", "3" } };
		
		
		String[][] sBox6 = { 
				{"23","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"},
				{"00","12", "1", "10", "15", "9", "2", "6", "8", "0", "13", "3", "4", "14", "7", "5", "11" },
				{"01", "10", "15", "4", "2", "7", "12", "9", "5", "6", "1", "13", "14", "0", "11", "3", "8" },
				{"10", "9", "14", "15", "5", "2", "8", "12", "3", "7", "0", "4", "10", "1", "13", "11", "6" },
				{"11","4", "3", "2", "12", "9", "5", "15", "10", "11", "14", "1", "7", "6", "0", "8", "13" } };
		
		String[][] sBox7 = { 
				{"23","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"},
				{ "00","4", "11", "2", "14", "15", "0", "8", "13", "3", "12", "9", "7", "5", "10", "6", "1" },
				{ "01","13", "0", "11", "7", "4", "9", "1", "10", "14", "3", "5", "12", "2", "15", "8", "6"},
				{ "10","1", "4", "11", "13", "12", "3", "7", "14", "10", "15", "6", "8", "0", "5", "9", "2" },
				{ "11","6", "11", "13", "8", "1", "4", "10", "7", "9", "5", "0", "15", "14", "2", "3", "12" } };
		
		String[][] sBox8 = { 
				{"23","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"},
				{"00",  "13",   "2",   "8",   "4",   "6",  "15",  "11",   "1",  "10",   "9",   "3",  "14",   "5",   "0",  "12", "7" },
				{"01",   "1",  "15",  "13",   "8",  "10",   "3",   "7",   "4",  "12",   "5",   "6",  "11",   "0",  "14",   "9", "2" },
				{"10",   "7",  "11",   "4",   "1",   "9",  "12",  "14",   "2",   "0",   "6",  "10",  "13",  "15",   "3",   "5", "8" },
				{"11",   "2",   "1",  "14",   "7",   "4",  "10",   "8",  "13",  "15",  "12",   "9",  "0",    "3",   "5",   "6", "11" } };
		
		if (box==1) {
			result = sBox1;
		}
		if (box==2) {
			result = sBox2;
		}
		if (box==3) {
			result = sBox3;
		}
		if (box==4) {
			result = sBox4;
		}
		if (box==5) {
			result = sBox5;
		}
		if (box==6) {
			result = sBox6;
		}
		if (box==7) {
			result = sBox7;
		}
		if (box==8) {
			result = sBox8;
		}
		
		
		
		
		return result;
	
		
	}
	

	public static String  calculatesbox(String str6, int box) {
		
		String result ="";
	String [][] sbox = getsbox(box);
	
	
	String row = Character.toString(str6.charAt(0))+ Character.toString(str6.charAt(str6.length()-1));
		String column = str6.substring(1, 5);

		for (int i = 0; i < sbox.length; i++) {
			
			if (sbox[i][0].equals(row)) {
				
			for (int j = 0; j < sbox[0].length; j++) {
				
				if (sbox[0][j].equals(column)) {
					result = String.valueOf(sbox[i][j]);
				}
			}
		}
			
		}
		
		return result;
	
		
	}
	public static String calculatesboxcomplete(String str48){    
		String result  ="";
		
		String str1 =str48.substring(0, 6);
		String str2 =str48.substring(6, 12);
		String str3 =str48.substring(12, 18);
		String str4 =str48.substring(18, 24);
		String str5 =str48.substring(24, 30);
		String str6 =str48.substring(30, 36);
		String str7 =str48.substring(36, 42);
		String str8 =str48.substring(42, 48);
		
		result +=dectobin(calculatesbox(str1, 1));
		result +=dectobin(calculatesbox(str2, 2));
		result +=dectobin(calculatesbox(str3, 3));
		result +=dectobin(calculatesbox(str4, 4));
		result +=dectobin(calculatesbox(str5, 5));
		result +=dectobin(calculatesbox(str6, 6));
		result +=dectobin(calculatesbox(str7, 7));
		result +=dectobin(calculatesbox(str8, 8));
		
		
		
		return result;
		
	}
	
	
	public static String dectobin(String dec) {
		String result  ="";
		if (dec.equals("0")) {result= "0000";}
		if (dec.equals("1")) {result= "0001";}
		if (dec.equals("2")) {result= "0010";}
		if (dec.equals("3")) {result= "0011";}
		if (dec.equals("4")) {result= "0100";}
		if (dec.equals("5")) {result= "0101";}
		
		if (dec.equals("6")) {result= "0110";}
		if (dec.equals("7")) {result= "0111";}
		if (dec.equals("8")) {result= "1000";}
		if (dec.equals("9")) {result= "1001";}
		if (dec.equals("10")) {result= "1010";}
		
		if (dec.equals("11")) {result= "1011";}
		if (dec.equals("12")) {result= "1100";}
		if (dec.equals("13")) {result= "1101";}
		if (dec.equals("14")) {result= "1110";}
		if (dec.equals("15")) {result= "1111";}
		
		
		
		return result;
		
		
	
	}
	
	
	
	
	
	
	
	
	
	public  static String getinipermutpc1(String str) {
		String result ="";
		int [] pc1={58,50,42,34,26,18,10,2,60,52,44,36,28,
				20,12,4,62,54,46,38,30,22,14,6,64,56,48,40,32,24,16,8,
				57,49,41,33,25,17,9,1,59,51,43,35,27,19,11,3,
				61,53,45,37,29,21,13,5,63,55,47,39,31,23,15,7};
	 for(int i=0;i<pc1.length;i++) {
		
	  result += str.charAt(pc1[i]-1);
			}
	
	 return result;
	}
	
	public static String getblockr(String str,int round,String key) {
	String	result ="";
		
		String permutinit =getinipermutpc1(str);
		String rblock = "";
		rblock = permutinit.substring(32);
		
		String lblock = "";
		lblock = permutinit.substring(0, 32);
		
		for (int i = 0; i < round; i++) {
			String expansion= getepansion(rblock);
			String gexort=xorbin(expansion, key);
			String getsbox=calculatesboxcomplete(gexort);
			
			String endpermit =finpermutation(getsbox);
					
				 result = xorbin(lblock, endpermit);
				
				lblock= rblock;
				rblock= result;
		}
		
		
	
		return result;
	}

	

	public static String calculatround (String lb, String rb,String key, int round) {return rb+ feistefunction(lb+rb, DesKeySchedule(key, round));}
	
	
	
	
	public static String getasciicode(char plain) {
		String result= "";
	
		switch (plain) {
	case 'A': result="01000001";break;                case 'a': result="01100001";break;
	case 'B': result="01000010";break;                case 'b': result="01100010";break;
	case 'C': result="01000011";break;                case 'c': result="01100011";break;
	case 'D': result="01000100";break;                case 'd': result="01100100";break;
	case 'E': result="01000101";break;                case 'e': result="01100101";break;
	case 'F': result="01000110";break;                case 'f': result="01100110";break;
	case 'G': result="01000111";break;                case 'g': result="01100111";break;
	case 'H': result="01001000";break;                case 'h': result="01101000";break;
	case 'I': result="01001001";break;                case 'i': result="01101001";break;
	case 'J': result="01001010";break;                case 'j': result="01101010";break;
	case 'K': result="01001011";break;                case 'k': result="01101011";break;
	case 'L': result="01001100";break;                case 'l': result="01101100";break; 
	case 'M': result="01001101";break;                case 'm': result="01101101";break; 
	case 'N': result="01001110";break;                case 'n': result="01101110";break;
	case 'O': result="01001111";break;                case 'o': result="01101111";break;
	case 'P': result="01010000";break;                case 'p': result="01110000";break;
	case 'Q': result="01010001";break;                case 'q': result="01110001";break;
	case 'R': result="01010010";break;                case 'r': result="01110010";break;
	case 'S': result="01010011";break;                case 's': result="01110011";break;
	case 'T': result="01010100";break;                case 't': result="01110100";break;
	case 'U': result="01010101";break;                case 'u': result="01110101";break;
	case 'V': result="01010110";break;                case 'v': result="01110110";break;
	case 'W': result="01010111";break;                case 'w': result="01110111";break; 
	case 'X': result="01011000";break;                case 'x': result="01111000";break;
	case 'Y': result="01011001";break;                case 'y': result="01111001";break;
	case 'Z': result="01011010";break;                case 'z': result="01111010";break; 
	
		}
		return result;
		
	
	}
	
	
	 public static List <String> getasciicomplet1(String str) {
			
			List <String> code = new ArrayList<>();
			
			for (int i = 0; i < str.length(); i++) {
			
				code.add(getasciicode(str.charAt(i)));
			}
			return code;
		}
			 public static String encryptrc4(String key, String plain) {return getasciiixor(getasciicomplet1(plain), GenRc4LoopEN(KeySchedule(key), plain)); }
			
			
			
			
			
			   public static int[] GenRc4LoopEN(String state, String cleartext){
					int[] result = new int[cleartext.length()];
					int[] statearray = movetrich(state);
					
					int j = 0;
					int i = 0;
					for (int k = 0;  k  < cleartext.length();  k++){
						i = moduloint(i + 1 , statearray.length);
						j = moduloint(j + statearray[i], statearray.length);
						
						statearray= Swaping(i, j, statearray);
						int	b = moduloint(statearray[i] + statearray[j], statearray.length);
						
						result[k] = statearray[b];
					}
				
					return result;
			   }
	
	
			   
			   public static String rsaencrypt(int key, int e,String clear) {
			    	String result ="";
			    	int cache =0;
			    	for (int i = 0; i <clear.length(); i++) {
						cache = bintoint(getasciicode(clear.charAt(i)));
						int r =1;
						for (int j = 0; j <e; j++) {
							r=r*cache;
							r= moduloint(r, key);
						}
						String resultstr =modulo(r, key);
						
						if (i==0) {
							result = resultstr;
						} else {
							result+='_'+resultstr;
						}
						
						
					}
					return result;
			    	
			    }
			   
			    public static int power(int zahl,int pui) {

			        int count = pui;
			        int result =zahl;
			        if(count ==0) {
			            result =1;
			        }


			        while(count>1) {
			            result *=zahl;
			            count--;
			        }
			        return result;
			    }
			
			   public static int bintoint (String c) {

			        
			        int result= 0;


			        int count= c.length()-1;

			        int countpui=0;
			        
			        
			        while (count>=0) {

			            if(c.charAt(count)=='0') {

			            }else {
			                result= result + power(2,countpui)  ;
			            }

			            count--;
			            countpui++;
			        }


			        return  result;

			    } 
			   
			   
			   
			   
			   
			   
	
	 //////////////////////////////////////////////////////noch nicht gel�st////////////////////////////////////////////////////
	
	
			   
		public static String dectobinaire(int dec ,String s) {
			
			
			String result =s;
			
			if (dec>0) {
				
			
			if (modulo(dec, 2).equals("0")) {
				result+='0';
				return dectobinaire(dec/2, result);
			} else {
				result+='1';
				return dectobinaire(dec/2, result);
			}
			}else {
				return gettdectobinare(result);
			}

			
		}
		
		public static String gettdectobinare(String inverse) {
			String result ="";
			
			for (int i = inverse.length()-1; i >=1; i--) {
				result += inverse.charAt(i);
				
			}
			return result;
			
		}
			
		 public static int genpubkeyrsa() {
		    	int number1 =getprim(0);
		    	int number2=getprim(number1);
		    	
		    	int n = number1*number2;
		    	int fi = (number1-1)*(number2-1);
		    	
		    		   int cache = getgcd(fi, fi);
		    	
				return cache;
		    	
		    }
		  public static int exteneuclide(int number,int number1) {
			  
			return number1;
			  
		  }
		    
		    
		    public static int getgcd(int fi, int ediv) {
		    	Random r = new Random();
		    	int e= r.nextInt(ediv);
		    	if (gcd(e, fi)!=1) {
		    		return getgcd(fi,e);
				} else {
					return e;
				}
			
		    	
		    }
		    public static int gcd(int number1, int number2) {
		    	int result =1;
		    	 List<String>liste= faktorisierunggdc(number1);
		    	 List<String>liste2= faktorisierunggdc(number2);
		    	 for (int i = 0; i < liste.size(); i++) {
					for (int j = 0; j < liste2.size(); j++) {
						if (liste.get(i).equals(liste2.get(j))) {
							result =0;
						}
					}
				}
				return result;
		    	
		    }
		    public static List<String> faktorisierunggdc (int number) {

		    	   String resultstr= "";
	 List<String>liste =new ArrayList<>();
		    	   
		    	   if(primzahl(number)==0){
		    		   liste.add(String.valueOf(number));
		    		   resultstr =String.valueOf(number);
		    		   
		    	   }else if(primzahl(number)!=1){
		    		   
		    		   resultstr =String.valueOf(primzahl(number));
		    		   
		    		   int number2= number /primzahl(number);
		    		   while(number2>0) {
		    			   
		    			   if(primzahl(number2)==1) {
		    				   
		    				   resultstr += "*"+String.valueOf(number2);
		    				   liste.add(String.valueOf(number2));
		    			   }else {
		    			   resultstr += "*"+String.valueOf(primzahl(number2));
		    			   liste.add(String.valueOf(number2));
		    			   }
		    			   
		    			   
		    			   if(primzahl(number2)==1) {
		    				  number2 =0; 
		    			   }else {
		    			   number2= number2 / primzahl(number2);
		    			   }
		    		   }
		    		   
		    	   }
		    	   
		    	   

		    	   return liste;
		    	}
		    public static  int  getprim(int number) {
				   Random r = new Random();
				   
				   int result=0;
				   result=r.nextInt(100);
				   if (primzahl(result)!=1) {
					   getprim(number);
				}
				   if (number!=0) {
					
					   if (number == result) {
						   getprim(number);
					}
				}
				return result;
				   
			   }    
}
